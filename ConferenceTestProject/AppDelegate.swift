//
//  AppDelegate.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = makeViewController()
        window?.makeKeyAndVisible()
        
        return true
    }
    
    private func makeViewController() -> ViewController {
        let viewController = ViewController()
        let possibleConferencePeriodsAnalizer = PossibleConferencePeriodsAnalizerImpl()
        let bestConferenceDateAnalizer = BestConferenceDateAnalizerImpl(possibleConferencePeriodsAnalizer)
        viewController.interactor = CountryConferencesInteractorImpl(bestConferenceDateAnalizer)
        viewController.conferenceInfoRepository = ConferenceInfoRepositoryImpl()
        viewController.attendeeRepository = AttendeeRepositoryImpl()
        return viewController
    }

}

