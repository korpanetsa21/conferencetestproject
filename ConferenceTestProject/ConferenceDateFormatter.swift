//
//  ConferenceDateFormatter.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation

final class ConferenceDateFormatter: DateFormatter {
    
    override init() {
        super.init()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.dateFormat = "yyyy-MM-dd"
        self.locale = Locale.us
        self.timeZone = TimeZone(secondsFromGMT: 0)
    }
    
}
