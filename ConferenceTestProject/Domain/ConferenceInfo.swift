//
//  ConferenceInfo.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation


struct ConferenceInfo: Encodable {
    
    let country: String
    let startingDate: Date
    let emails: [String]
    
}
