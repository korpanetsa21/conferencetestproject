//
//  Attendee.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation

struct Attendee: Decodable {
    
    let firstName: String
    let lastName: String
    let email: String
    let country: String
    let availableDates: [Date]
    
}
