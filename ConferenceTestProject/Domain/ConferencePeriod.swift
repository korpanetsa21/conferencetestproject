//
//  ConferencePeriod.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation

struct ConferencePeriod: Hashable {
    
    let start: Date
    let end: Date
    
}

extension ConferencePeriod: Equatable {
    
    static func == (lhs: ConferencePeriod, rhs: ConferencePeriod) -> Bool {
        let calendar = Calendar.current
        return calendar.compare(lhs.start, to: rhs.start, toGranularity: .day) == .orderedSame &&
            calendar.compare(lhs.end, to: rhs.end, toGranularity: .day) == .orderedSame
    }
    
}
