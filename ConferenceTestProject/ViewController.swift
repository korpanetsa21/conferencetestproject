//
//  ViewController.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var interactor: CountryConferencesInteractor!
    var conferenceInfoRepository: ConferenceInfoRepository!
    var attendeeRepository: AttendeeRepository!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        do {
            let attemdees = try attendeeRepository.fetch()
            print(attemdees.count)
            let result = interactor.perform(attemdees)
            let url = try conferenceInfoRepository.save(result)
            print(url)
        } catch {
            print(error)
        }
    }

}

