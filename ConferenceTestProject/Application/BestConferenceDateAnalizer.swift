//
//  BestConferenceDateAnalizer.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation

struct SearchResult {
    
    let period: ConferencePeriod
    let attendees: [Attendee]
    
}

protocol BestConferenceDateAnalizer {
    
    func perform(input: [Attendee]) -> SearchResult?
    
}

final class BestConferenceDateAnalizerImpl: BestConferenceDateAnalizer {
    
    private let periodsAnalizer: PossibleConferencePeriodsAnalizer
    
    init(_ periodsAnalizer: PossibleConferencePeriodsAnalizer) {
        self.periodsAnalizer = periodsAnalizer
    }
    
    func perform(input: [Attendee]) -> SearchResult? {
        var temp: [ConferencePeriod: [Attendee]] = [:]
        input.forEach { attendee in
            let timePeriods = periodsAnalizer.perform(attendee.availableDates)
            timePeriods.forEach({ timePeriod in
                if let attendees = temp[timePeriod] {
                    temp[timePeriod] = attendees + [attendee]
                } else {
                    temp[timePeriod] = [attendee]
                }
            })
        }
        var result: SearchResult?
        for timePeriod in temp.keys {
            let attendees = temp[timePeriod] ?? []
            if let presiousResult = result {
                if attendees.count > presiousResult.attendees.count || (attendees.count == presiousResult.attendees.count && timePeriod.start < presiousResult.period.start) {
                    result = SearchResult(period: timePeriod, attendees: attendees)
                }
            } else {
                result = SearchResult(period: timePeriod, attendees: attendees)
            }
        }
        return result
    }
    
}
