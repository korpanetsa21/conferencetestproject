//
//  CountryConferencesInteractor.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation

protocol CountryConferencesInteractor {
    
    func perform(_ input: [Attendee]) -> [ConferenceInfo]
    
}

final class CountryConferencesInteractorImpl: CountryConferencesInteractor {
    
    private let bestConferenceDateAnalizer: BestConferenceDateAnalizer
    
    init(_ bestConferenceDateAnalizer: BestConferenceDateAnalizer) {
        self.bestConferenceDateAnalizer = bestConferenceDateAnalizer
    }
    
    func perform(_ input: [Attendee]) -> [ConferenceInfo] {
        let attendeesByCountries = Dictionary(grouping: input, by: { $0.country })
        var result: [ConferenceInfo] = []
        for country in attendeesByCountries.keys {
            guard let attendees = attendeesByCountries[country], let periodSearchResult = bestConferenceDateAnalizer.perform(input: attendees) else { continue }
            result.append(ConferenceInfo(country: country, startingDate: periodSearchResult.period.start, emails: periodSearchResult.attendees.map({ $0.email })))
        }
        return result
    }
    
}
