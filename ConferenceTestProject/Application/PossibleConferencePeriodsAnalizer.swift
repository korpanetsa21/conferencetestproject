//
//  PossibleConferencePeriodsAnalizer.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation

protocol PossibleConferencePeriodsAnalizer {
    
    func perform(_ input: [Date]) -> [ConferencePeriod]
    
}

final class PossibleConferencePeriodsAnalizerImpl: PossibleConferencePeriodsAnalizer {
    
    func perform(_ input: [Date]) -> [ConferencePeriod] {
        var result: [ConferencePeriod] = []
        let dateList = input.sorted(by: <)
        let calendar = Calendar.current
        for (index, date) in dateList.enumerated() {
            if date != dateList.last {
                if let nextDate = calendar.date(byAdding: .day, value: 1, to: date), calendar.isDate(nextDate, inSameDayAs: dateList[index + 1]) {
                    result.append(ConferencePeriod(start: date, end: dateList[index + 1]))
                }
            }
        }
        return result
    }
    
}
