//
//  Locale+US.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation

extension Locale {
    
    static var us = Locale(identifier: "en_US_POSIX")
    
}
