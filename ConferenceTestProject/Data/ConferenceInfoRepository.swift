//
//  ConferenceInfoRepository.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation

protocol ConferenceInfoRepository {
    
    func save(_ info: [ConferenceInfo]) throws -> URL
    
}

final class ConferenceInfoRepositoryImpl: ConferenceInfoRepository {
    
    func save(_ info: [ConferenceInfo]) throws -> URL {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(ConferenceDateFormatter())
        let data = try encoder.encode(info)
        let url: URL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("file.json")
        try data.write(to: url)
        return url
    }
    
}
