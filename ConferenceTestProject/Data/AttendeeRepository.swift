//
//  AttendeeRepository.swift
//  ConferenceTestProject
//
//  Created by Taras Korpanets on 9/6/19.
//  Copyright © 2019 taraskorpanets. All rights reserved.
//

import Foundation

enum AttendeeRepositoryError: Error {
    case fileNotFound
}

protocol AttendeeRepository {
    
    func fetch() throws -> [Attendee]
    
}

final class AttendeeRepositoryImpl: AttendeeRepository {
    
    func fetch() throws -> [Attendee] {
        guard let url = Bundle.main.url(forResource: Constant.fileName, withExtension: "json") else {
            throw AttendeeRepositoryError.fileNotFound
        }
        let data = try Data(contentsOf: url)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(ConferenceDateFormatter())
        return try decoder.decode([Attendee].self, from: data)
    }
    
}

private extension AttendeeRepositoryImpl {
    
    enum Constant {
        static let fileName = "TestNew"
    }
    
}
